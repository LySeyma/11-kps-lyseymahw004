package com.company;
import java.util.ArrayList;
import java.util.List;
public class Wrapper<E> {
    private int size;
    private List<E> set =new ArrayList<>();
    public Wrapper(){
        size=0;
    }
//    ----funtion for add item into ArrayList----
    public void addItem(E value) throws DuplicateException{
        set.add(value);
//        ---handel null value---
        if (value == null){
            throw new NumberFormatException("Inputted null Value !");
        }
//        ---handel duplicates exception ---
        if (hasDuplicates()) {
            throw new DuplicateException("Duplicate Value :" + value);
        }
        size++;
    }
//    ---funtion for get intem from arraylist---
    public E getItem(int index) {
        return set.get(index);
    }
    public int size() {
        return size;
    }
//   --- funtion for handel duplicates---
    public boolean hasDuplicates(){
        int count = set.size();
        E E1,E2;

        for(int i=0;i<count;i++){
            E1 = set.get(i);
            for(int j=i+1;j<count;j++){
                E2 = set.get(j);
                if(E2.equals(E1)){
                    return true;
                }
            }
        }
        return false;
    }
}
